%
% CHAPTER: Inaccuracy
%

\chapterimage{Train_wreck_at_Montparnasse_1895.pdf} % Chapter heading image

\chapter{Inaccuracy}
\label{chap:Error}

\begin{quote}
\begin{flushright}
\emph{A little inaccuracy sometimes saves\\
tons of explanations.}\\
Saki
\end{flushright}
\end{quote}
\bigskip

In Section \ref{def:descriptions_model} we introduced the concept of model $m$ of a representation $r$ as a Turing machine that when it is interpreted by a universal Turing machine $UT$ it prints out the string $r$. However, in practice, and since our knowledge about the topic is still incomplete, it could happen that $UT(m)$ produces the string $r'$, which is similar to the string $r$, but not equal. In this chapter we are interested in to study how close the string $r'$ is to the original string $r$. We call this quantity \emph{inaccuracy}.

Inaccuracy is the second element we will use to measure how well we understand a topic. The intuition is that the more accurate is our model, the better we understand the topic. From a formal point of view, the inaccuracy of a description is measured as the normalized information distance between the original representation $r$ and the representation produced by our model $r'$. Inaccuracy compares the output $r'$ of our inaccurate model $m$ with a representation $r$, however, $r$ could be an incorrect representation (positive miscoding) of the original entity $e$.

Inaccuracy is a quantity that it is no computable in general, and so, it must be approximated in practice. Unfortunately, it does not exists an approximation that can by applied to all kind of topics (strings, abstract, ...) and so, different alternatives will be provided and studied in Part III of this book. 

In this chapter will also introduce the concepts of joint inaccuracy of two topics and the conditional inaccuracy of a topic given a perfect knowledge of another topic. And finally, we will extend the concept of inaccuracy from individual topics to research areas.

%
% Section: Inaccuracy
%
\section{Inaccuracy}
\label{sec:error}

When studying a topic $r \in \mathcal{R}$ it might happen that we are using a model $m$ that it is not a valid model of $r$, that is, $m \notin \mathcal{M}_r$ (see Definition \ref{def:descriptions_model}). In this case, $UM(m)$ would be a string $r' \in \mathcal{B}^\ast$ different from the string $r$. When this happens we say that the model $m$ is \emph{inaccurate} when describing $r$.

\begin{definition}
Let $r \in \mathcal{R}$ a representation, and $m = \langle TM, a \rangle$ a model. If $TM(a) = r'$, where $r' \in \mathcal{B}^\ast$ and $r \neq r'$, we say that the model $m$ is \emph{inaccurate} for the representation $r$.
\end{definition}

From a formal point of view we can not require that inaccurate models belong to $\mathcal{M}_\mathcal{R}$, since it might happen that $r'$ does no belong to $\mathcal{R}$. Intuitively, $r'$ would be something close to a representation, but not necessarily a representation.

If a model is inaccurate, we would like to have a quantitative measure of how far we are from describing the right topic. In terms of computability, a natural way to define this measure would be by means of computing normalized information distance between the original representation $r$ and the representation $r'$ generated by our model.

\begin{definition} [Inaccuracy]
\label{def:inaccuracy}
Let $r \in \mathcal{R}$ be a representation, and $m$ a model. We define the \emph{inaccuracy of the model} $m$ for the representation $r$, denoted by $\iota(m, r)$, as:
\[
\iota(m, r) = \frac{ \max\{ K \left(r \mid \Gamma(m) \right), K \left( \Gamma(m) \mid r \right) \} } { \max\{ K(r), K \left(\Gamma(m) \right) \} }
\]
\end{definition}

Having a relative measure of inaccuracy instead of an absolute one allow us to compare the inaccuracy of different models for the same representation, and even to compare the inaccuracy of different representations.

\begin{example}
\label{ch1:ex:newton}
The topic Newton's second law of motion $F = m a$ could be encoded given the results of an experiment using objects of different masses to which we apply different forces and then we measure the acceleration. The dataset would be composed by the masses, the forces, and the accelerations achieved for each combination of mass and force. However, if we are interested in the acceleration due to gravity, forces and masses cancel out, and so, the only value to measure is acceleration.  Of course, if the result of the experiment is a large collection of measurements, the encoding of the entity would be very long. But, as we said before, with the encodings of entities we are not interested in to find the shortest possible strings, what we are looking for are complete and accurate encodings of topics.

For this example we will use the results of an experiment performed by the National Bureau of Standards in Washington D.C. between May 1934 and July 1935. The dataset is composed by 81 measurements, and each value is expressed in centimeters per second squared, for example 980,078. In practice we approximate the quantity $\frac{ K(t \mid d_t) } {K(t)}$ by $\frac{ l(D \mid M) } {l(D)}$, where $l(D \mid M)$ is the length of the data given the model, and $l(D)$ is the length of the data. In order to encode the dataset we require 20 bits per measure (using an uniform code, as it is explained in Chapter \ref{chap:Coding}), and so, to encode the full dataset we require 1,620 bits. If we assume that our model predicts a gravity of $980,000 cm/s^2$ plus a random error that follows a normal distribution (estimated using a maximum likelihood approach), we have that it requires 453 bits to encode the data given the model (see Chapter \ref{chap:Discrete_Probability} for more information about how to encode a dataset given a model). We have that error of the model is $\frac{453}{1,620} = 0.27$.
\end{example}

The inaccuracy of a representation is, conveniently, a number between $0$ and $1$, as next proposition shows.

\begin{proposition}
\label{prop:range_error}
We have that $0 \leq \iota(m_t) \leq 1$ for all $t \in \mathcal{T}$ and $m_t \in \mathcal{M}$.
\end{proposition}
\begin{proof}
Given that $K(t \mid m_t)>0$ and that $K(t)>0$, since they are the lengths of non-empty strings, and that $K(t \mid m_t)  \geq K(t)$ as it was proved in Proposition \ref{prop:kolmogorov_conditional}.
\end{proof}

Inaccuracy is a quantity that cannot be computed in general, and so, it must be approximated. How to approximate the concept of inaccuracy is something that depends on the characteristics of the topics under study. If the topics are strings themselves, the weak error could be based on strings distance metrics, like for example the normalized compression distance. If topics are abstract entities, like mathematical concepts, their descriptions would be the result of an experiment, and so, the weak error could be based on the normalized length of the dataset given the model $l(D \mid M) / l(D)$, using the approach described in Section \ref{sec:MDL}. In Part III of this book we will see several examples of approximations.

%
% Section: Joint Inaccuracy
%

\section{Joint Inaccuracy}

In Section \ref{sec:descriptions_joint_topic} we introduced the concept of joint representation as a exploratory mechanism to discover new research entities, and in Section \ref{sec:joint_miscoding} we saw how the metric of miscoding changes when concatenate two representations. In this section we are going to study how inaccuracy varies when concatenating representations.

Given two representations $r$ and $s$, we want to know the inaccuracy of the model $m$ when describing the joint representation $rs$. Since we required that $rs$ must be a valid representation (see Definition \ref{def:descriptions_extended_topics}), the formalization of the concept of joint inaccuracy is straightforward:

\begin{definition}
\label{def:inaccuracy_joint_representation}
Let $r, s \in \mathcal{R}$ be two different representations, and $m$ a description. We define the joint inaccuracy of the model $m$ for the joint topic $rs$, denoted by $\iota(m, rs)$, as:
\[
\iota(m, rs) = \frac{ \max\{ K \left(rs \mid \Gamma(m) \right), K \left( \Gamma(m) \mid rs \right) \} } { \max\{ K(rs), K \left(\Gamma(m) \right) \} }
\]
\end{definition}

{\color{red} TODO: Introduce the following propostion}

\begin{proposition}
Let $r \in \mathcal{R}$ be a representation and $m \in \mathcal{M}$ be a model, then we have that $0 \leq \iota(m, r) \leq 1$.
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

{\color{red} TODO: Introduce the following propostion}

\begin{proposition}
Let $r, s \in \mathcal{R}$ be two different representations, and $m \in \mathcal{M}$ be a model, then we have that:
\[
\iota(m, rs) \leq \iota(m, r) + \iota(m, s)
\]
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

{\color{red} TODO: Introduce the following propostion}

\begin{proposition}
Let $r, s \in \mathcal{R}$ be two different representations, and $m \in \mathcal{M}$ be a model, then we have that:
\[
\iota(m, rs) \leq \iota(m, r), \iota(m, rs) \leq \iota(m, s)
\]
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

% TODO: i(m, ts) = i(m, st)

{\color{red} TODO: Introduce the following propostion}

\begin{proposition}
Let $r \in \mathcal{R}$ be a representation and $m \in \mathcal{M}$ be a model, then we have that:
\[
\iota(m, rs) = \iota(m, sr)
\]
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

\begin{example}
\label{ex:inaccuracy_joint_inequality}
{\color{red} TODO: Provide an example}
\end{example}

{\color{red} TODO: Fix the following definitions and propositions}

We can extend the concept of joint miscoding to any collection, but finite, of topics.

\begin{definition}
Let $t_1, t_2, \ldots, t_n \in \mathcal{T}$ a finite collection of topics. We define the \emph{joint miscoding of} $t_1, t_2, \ldots, t_n$, denoted by $\mu(t_1, t_2, \ldots, t_n)$, as:
\[
\mu(t_1, t_2, \ldots, t_n) = \min_{(t_{e_1}, t_{e_2}, \ldots, t_{e_n}) \in \mathcal{T}_\mathcal{E}^n}  \frac{K \left( \langle t_{e_1}, t_{e_2}, \ldots, t_{e_n} \rangle \mid \langle t_1, t_2, \ldots, t_n \rangle \right) }{K \left( \langle t_{e_1}, t_{e_2}, \ldots, t_{e_n} \rangle \right)}
\]
We define the \emph{weak miscoding of} $t_1, t_2, \ldots, t_n$, denoted by $\mu(t_1 t_2 \ldots t_n)$, as:
\[
\mu(t_1 t_2 \ldots t_n) = \min_{t_e \in \mathcal{T}_\mathcal{E}} \frac{K(t_e \mid t_1 t_2 \ldots t_n)}{k(t_e)}
\]
\end{definition}

It is easy to prove that the properties of the concept of miscoding generalizes to multiple topics.

\begin{proposition}
Let $t_1, t_2, \ldots, t_n \in \mathcal{T}$ a finite collection of topics. Then, we have that:

\renewcommand{\theenumi}{\roman{enumi}}
\begin{enumerate}
\item $\mu(t_1, t_2, \ldots, t_n) \leq \mu(t_i) \; \forall \, 0 \leq i \leq n$,
\item $\mu(t_1, \ldots, t_i, \ldots, t_j, \ldots, t_n) = \mu(t_1, \ldots, t_j, \ldots, t_i, \ldots, t_n) \; \forall \, 0 \leq i \leq j \leq n$.
\item $\mu(t_1 t_2 \ldots t_n) \leq \mu(t_1, t_2, \ldots, t_n)$.
\end{enumerate}
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

% Inaccuracy of Joint Models

\subsection{Inaccuracy of Joint Models}

{\color{red} TODO: Do we need this concept?}

Given two topics $t$ and $s$, instead of talking about the inaccuracy of the joint topic $ts$, we prefer to deal with the joint inaccuracy of the individual topics.

\begin{definition}
Let $t, s \in \mathcal{T}$ be two different topics. We define the \emph{joint inaccuracy of the topics $t$ and $s$ given the models $m_s$ and $m_s$}, denoted by $\iota(m_t, m_s)$, as:
\[
\iota( m_t, m_s ) = \frac{ K(ts \mid  \langle m_t, m_s \rangle ) } {K(ts)}.
\]
\end{definition}

As it was the case of the concept of miscoding, joint inaccuracy is a number between $0$ and $1$.

\begin{proposition}
We have that $0 \leq \iota(m_t, m_s) \leq 1$ for all $t, s \in \mathcal{T}$ and all $m_t, m_s$.
\end{proposition}
\begin{proof}
{\color{red} Review}
Given that $K \left( \langle t, s \rangle \mid d_{t,s} \right)>0$ and that $K \left( \langle t, s \rangle \right)>0$, since they are the lengths of non-empty strings, and that $K \left( \langle t, s \rangle \mid d_{t,s} \right)  \geq K \left( \langle t, s \rangle \right)$ as it was proved in Proposition \ref{prop:kolmogorov_conditional}.
\end{proof}

{\color{red} TODO: Introduce the following propostion}

\begin{proposition}
Let $t$ and $s$ be two different topics, and $m_t$ and $m_s$ two models. We have that:
\[
\iota(m_t, m_s) \leq \iota(m_t) + \iota(m_s)
\]
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

{\color{red} TODO: Introduce the following propostion}

\begin{proposition}
Let $t$ and $s$ be two different topics, and $m_t$ and $m_s$ two models. We have that:
\[
\iota(m_t, m_s) \geq \iota(m_t)
\]
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

When dealing with the joint inaccuracy of two topics, the order in which the topics are listed is not relevant.

\begin{proposition}
We have that $\iota(m_t, m_s) = \iota(m_s, m_t)$ for all $t,s \in \mathcal{T}$ and all $m_t, m_s$.
\end{proposition}
\begin{proof}
{\color{red} Review}
Apply Propositions \ref{prop:kolmogorov_order} and \ref{prop:joint_order}.
\end{proof}

%
% Section: Conditional Inaccuracy
%

\section{Conditional Inaccuracy}

We are also interested into study the inaccuracy of a topic given a perfect knowledge of another topic. That is, we would like to know the \emph{conditional inaccuracy} of a topic.

\begin{definition}
Let $t,s \in \mathcal{T}$ be two different topics, and let $m_{t \mid s^\star}$ be a conditional model of $t$ given a perfect model for $s$. We define the \emph{conditional inaccuracy} of topic $t$ given the conditional description $m_{t \mid s^\star}$, denoted by $\iota(t \mid m_{t \mid s^\star})$, as: 
\[
\iota( t \mid m_{t \mid s^\star}) = \frac{ K \left(t \mid m_{t \mid s^\star} \right) } {K(t \mid m^\star_s)}.
\]
\end{definition}

Conditional surfeit is a relative measure, and so, a number between $0$ and $1$.

\begin{proposition}
We have that $0 \leq \sigma(d_{t \mid s^\star}) \leq 1$ for all $t,s$.
\end{proposition}
\begin{proof}
Given that $K(t,s) \leq l(d_{t,s})$ we have that $\frac{K(t, s)}{l \left( d_{t,s} \right)} \leq 1$ and so, $1 - \frac{K(t, s)}{l \left( d_{t,s} \right)} \geq 0$. Also, since $\frac{K(t, s)}{l \left( d_{t,s} \right)} > 0$ (both quantities are positive integers), we have that $1 - \frac{K(t, s)}{l \left( d_{t,s} \right)} \leq 1$.
\end{proof}

Intuition tell us that the surfeit of a description could only decrease if we assume the background knowledge given by the description of another topic. This is because we require that this background knowledge must be a perfect description (it presents no surfeit). However, as it was the case of joint surfeit, we have to wait until Chapter \ref{chap:Nescience} to formalize this intuition.

{\color{red} TODO: Review}

Finally, we can extend our concepts of conditional surfeit and conditional redundancy to multiple, but fine, number of topics.

\begin{definition}
Let $t, s_1, s_2, \ldots, s_n \in \mathcal{T}$ be a finite collection of topics, and let $d_{t \mid s_1^\star, s_2^\star, \ldots,s_n^\star}$ any conditional description of $t$ given $s_1, s_2, \ldots, s_n$. We define the \emph{conditional surfeit} of the description $d_{t \mid s_1^\star, s_2^\star, \ldots,s_n^\star}$, denoted by $\sigma(d_{t \mid s_1^\star, s_2^\star, \ldots,s_n^\star})$, as: 
\[
\sigma(d_{t \mid s_1^\star, s_2^\star, \ldots,s_n^\star}) = 1 - \frac{K\left( t \mid s_1^\star, s_2^\star, \ldots,s_n^\star \right)}{l \left( d_{t \mid s_1^\star, s_2^\star, \ldots,s_n^\star} \right)}
\]
And the \emph{conditional redundancy} of the description $d_{t_1, t_2, \ldots, t_n}$, denoted by $\rho(d_{t_1, t_2, \ldots, t_n})$, as:
\[
\rho(d_{t_1, t_2, \ldots, t_n}) = 1 - \frac{K(d_{t_1, t_2, \ldots, t_n})}{l \left( d_{t_1, t_2, \ldots, t_n} \right)}
\]
\end{definition}

It is easy to show that the properties of conditional surfeit and conditional redundancy apply to the case of multiple topics as well.

%
% Section: Inaccuracy of Areas
%

\section{Inaccuracy of Areas}

The concept of inaccuracy can be extended to research areas in order to quantitative measure the amount of effort required to fix an inaccurate description of the area.

\begin{definition}
Let $A \subset \mathcal{T}$ be an area with known subset $\hat{A} = \{t_1, t_2, \ldots, t_n\}$, and let $d_{\hat{A}}$ be one of its descriptions. We define the \emph{inaccuracy of the description} $d_{\hat{A}}$ as:
\[
\iota(d_{\hat{A}}) = \frac{ K(\langle t_1, t_2, \ldots, t_n \rangle \mid d_{\hat{A}}) } {K(\langle t_1, t_2, \ldots, t_n \rangle)}.
\]
\end{definition}



