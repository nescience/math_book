This is the source text files of the book "The Mathematics of the Unknown". The
book is a comprehensive introduction to the Theory of Nescience, a radical new
idea that models what is unknown, and we are not aware it is unknown. Also,
the theory provides a powerful tool to discover interesting unknown things,
with applications to many different disciplines, such as academic research,
business, technology and sociology.

If you just want to read the book, refer to our web page
http://www.mathematicsunknown.com/.

If you want to contribute to the book, you are welcome. Please check the
open issues and contact with the author.

This is a free book. However, if you want to support this research, please
consider buying a copy of the book at https://leanpub.com/nescience.

(c) 2014 - 2017 R. Garcia Leiva. All rights reserved.
