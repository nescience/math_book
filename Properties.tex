%
% CHAPTER 8.- Properties of Nescience
%

\chapterimage{Philosophers.pdf}

\chapter{Advanced Properties}
\label{chap:Properties-Nescience}

\begin{quote}
\begin{flushright}
\emph{Invert, always invert.}\\
Carl Gustav Jacob Jacobi\\
\end{flushright}
\end{quote}
\bigskip

This chapter covers more advanced mathematical properties of the concept of nescience. This chapter can be safely skipped by those readers interested only in the applications of nescience. However, it is highly recommended to read it, since if provides a deeper understanding of what nescience is exactly.

{\color{red} Extend this introduction with a very short review of the topics covered in the chapter.}

%
% Section: Axioms
%

\section{Axioms of Science}

Since the time of David Hilbert, mathematics is about the study of the properties of abstract objects and their relations, without paying too much attention to what these objects are or represent. Mathematicians create (or discover) abstract frameworks of logic that can have multiple interpretations. It is up to applied scientists to provide those interpretations. In the same way, we can provide an abstract definition of the concept of nescience, and study its properties, without making any explicit reference to science nor the scientific method. In doing so, we loose the interpretability of our theory, but, on the other side, we could apply the same results to other disciplines.

In this section we are going to propose two different collection of axioms. In the strong version of the axioms we assume that perfect knowledge exists and it can be achieved, at least in theory. In the weak version of the axioms we characterize what we mean by perfect knowledge, but this knowledge might not exits, or it might not be achievable. The strong version of the axioms is intended for the derivation of algorithms for automated knowledge discovery to be used in practice. The weak version is mostly interesting from a mathematical/philosophical point of view.

% Weak Axioms

\subsection{Weak Axioms}

In the collection of Weak Axioms of Science we do not require that that there exists a perfect description for all entities, that is, the axiom of existence of minimum (an the related axiom of minimum inaccuracy) is considered to be false. Instead, what we do is that the Theorem \ref{th:perfect_knowledge} is considered itself an axiom. We provide a required and sufficient conditions to achieve perfect knowledge, but it might happen that for some entities such perfect knowledge is not achievable.

 We do not require that all the entities under study have a perfect description, since the set $\mathcal{E}$ might be uncountable. However, we assume that there exists a subset of entities of $\mathcal{E}$ for which perfect descriptions exists.

\begin{definition}[Weak Axioms of Science]

\emph{Science} is the 6-tuple $\left( \mathcal{E}, \Sigma, \mathcal{M}, \mathcal{O}, \mathcal{R}, N \right)$, where

\begin{itemize}
\item $\mathcal{E}$ is a non-empty set,
\item $\Sigma$ is an alphabet,
\item $\mathcal{M} \subset \Sigma^\ast$ is a prefix-free set of partial recursive functions,
\item $\mathcal{O} : \mathcal{M} \rightarrow \mathbb{R}^+$ is a universal oracle,
\item $\mathcal{R} \subset \mathcal{M} \times \mathcal{M}$ is an equivalence relation, and
\item $N : \mathcal{M} \rightarrow \mathbb{R}$ is the \emph{nescience} function.
\end{itemize}

\smallskip

that satisfies the following axioms:

\smallskip

\begin{enumerate}[label=(\roman*)]

\item $N(s) \geq 0 \; \forall s \in \mathcal{M}$,

\item if $s, t \in \mathcal{M}$ such that $s \, \mathcal{R} \, t$, $l(s) = l(t)$ and $\mathcal{O} (s) < \mathcal{O} (t)$ then $N(s) < N(t)$,

\item if $s, t \in \mathcal{M}$ such that $s \, \mathcal{R} \, t$, $l(s) < l(t)$ and $\mathcal{O} (s) = \mathcal{O} (t)$ then $N(s) < N(t)$,

\item Let $s \in \mathcal{D}$, we have that $N(s) = 0$ if, and only if, $\mathcal{O} (s) = 0$ and $\nexists t \in \mathcal{D}$ such that $s \mathcal{R} t$, $\mathcal{O} (t) = 0$ and $l(t) < l(s)$.

\end{enumerate}

\end{definition}

No all possible sets $\mathcal{E}$ one can think of are valid sets in the theory of nescice; recall we follow the standard ZFC set of axioms for set theory. The alphabet $\Sigma$ must be composed by a finite set of symbols, and by $\Sigma^\ast$ we denote its Kleene closure. The functions that compose the set $\mathcal{L}$ are in the form $f : \Sigma^\ast \rightarrow \Sigma^\ast$; they could be partial, they must be recursive and we assume a standard bijective Gödel encoding as elements of $\mathcal{L}$.

{\color{red} Explain why the concept of miscoding is not part of the axioms.}

The \emph{axiom of non-negativity} (i) states that how much we do not know has to be measured as a positive real. The \emph{axiom  of inaccuracy} (ii) deals with the case of having two models of the same length, in which we should prefer the less inaccurate. The \emph{axiom of surfeit} (iii) says that between two models that produce the same result, that is, two models equally accurate, we should always use the shortest one. And finally, the \emph{axiom of minimum unknown} (iv) states that for every possible model, there exist an equivalent model that provides perfect knwoledge (wich it is not necessarily unique).

% Strong Axioms of Science

\subsection{Strong Axioms of Science}

In the strong version of the axioms of science we require that perfect knowledge must exists for all entities, and so, only fully knowable topics are considered as the subject of scientific research. Non knowable entities might, or might not, exists, but they are not part of the scientific endeavor. Moreover, every valid description is associated to a knowable entity, even if what we are trying to understand is something complete different. That perfect knowledge exists does not means that there exist a generic algorithm to find that knowledge; in fact, it does not.

In this new collection of axioms we replace the axiom of perfect knowledge (that will be derived as a theorem) by two new and stronger axioms regarding the existence of perfect knowledge and zero inaccuracy.

\begin{definition}[Strong Axioms of Science]

\emph{Science} is the 6-tuple $\left( \mathcal{E}, \Sigma, \mathcal{D}, \mathcal{O}, \mathcal{R}, N \right)$, where

\begin{itemize}
\item $\mathcal{E}$ is a countable set,
\item $\Sigma$ is an alphabet,
\item $\mathcal{D} \subset \Sigma^\ast$ is a prefix-free set of partial recursive functions,
\item $\mathcal{O} : \mathcal{D} \rightarrow \mathbb{R}^+$ is a universal oracle,
\item $\mathcal{R} \subset \mathcal{D} \times \mathcal{D}$ is an equivalence relation, and
\item $N : \mathcal{D} \rightarrow \mathbb{R}$ is the \emph{nescience} function.
\end{itemize}

\smallskip

that satisfies the following axioms:

\smallskip

\begin{enumerate}[label=(\roman*)]

\item $N(s) \geq 0 \; \forall s \in \mathcal{D}$,

\item if $s, t \in \mathcal{D}$ such that $s \, \mathcal{R} \, t$, $l(s) = l(t)$ and $\mathcal{O} (s) < \mathcal{O} (t)$ then $N(s) < N(t)$,

\item if $s, t \in \mathcal{D}$ such that $s \, \mathcal{R} \, t$, $l(s) < l(t)$ and $\mathcal{O} (s) = \mathcal{O} (t)$ then $N(s) < N(t)$,

\item $\forall s \in \mathcal{D} \; \exists \, t \in \mathcal{D}$ such that $s \mathcal{R} t$ and $N(t) = 0$.

\item $\forall s \in \mathcal{D}$ if $N(s) = 0$ then $\mathcal{O}(s) = 0$.

\end{enumerate}

\end{definition}

{\color{red} Q: Perhaps it would be better to assume that $N$ is recursive and derive axiom (v) as a consequence.}

In this strong version of the axioms the set $\mathcal{E}$ of entities must be countable. The collection of partial recursive functions that compose $\mathcal{D}$ have the same form that in case of the regular axioms of science. The new \emph{axiom of zero unknown} (iv) states that for every possible description, there exist an equivalent description (which is not necessarily unique) that provides perfect knowledge, and the new \emph{axiom of zero inaccuracy} (v) says that perfect knowledge implies zero inaccuracy (the converse, in general, does not hold).

We have assumed as axioms the behavior of the nescience function in case of having two related topics with the same length and one of them with a smaller inaccuracy than the other ($l(s) = l(t)$ and $\mathcal{O} (s) < \mathcal{O} (t)$), and the case of two topics with the same inaccuracy and one of them shorter than the other ($l(s) < l(t)$ and $\mathcal{O} (s) = \mathcal{O} (t)$). In the next proposition we will consider the rest of the cases.

\begin{proposition}
\label{prop:properties_nescience}
Let $s, t \in \mathcal{L}$ such that $s \mathcal{R} t$, then
\begin{enumerate}[label=(\alph*)]
\item if $l(s) = l(t)$ and $\mathcal{O}(s) > \mathcal{O}(t)$ then $N(s) > N(t)$,
\item if $l(s) > l(t)$ and $\mathcal{O}(s) = \mathcal{O}(t)$ then  $N(s) > N(t)$,
\item if $l(s) = l(t)$ and $\mathcal{O}(s) = \mathcal{O}(t)$ then $N(s) = N(t)$,
\item if $l(s) > l(t)$ and $\mathcal{O}(s) > \mathcal{O}(t)$ then $N(s) > N(t)$, and
\item if $l(s) < l(t)$ and $\mathcal{O}(s) < \mathcal{O}(t)$ then $N(s) < N(t)$.
\end{enumerate}
\end{proposition}
\begin{proof}
(a) and (b) are a direct consequence of axioms (ii) and (iii) respectively, by interchanging $s$ and $t$.

(c) {\color{red} TBD}

(d) {\color{red} TBD}

(e) {\color{red} TBD}
\end{proof}

Unfortunately there is not too much we can say about the nescience of two related models $s$ and $t$ in the case that $l(s) < l(t)$ and $\mathcal{O} (s) > \mathcal{O} (t)$, and in case that $l(s) > l(t)$ and $\mathcal{O} (s) < \mathcal{O} (t)$. 

{\color{red} Mention the reverse of the above cases. Perhaps with a new proposition.}

According to the Axiom of non-negativity nescience is a value greater or equal than zero. Next theorem shows the necessary and sufficient conditions for the nescience to be exactly zero. The theorem is a characterization of what we mean to achieve a perfect knowledge about a research topic.

\begin{theorem}[Perfect Knowledge]
\label{th:perfect_knowledge}
Let $s \in \mathcal{D}$, we have that $N(s) = 0$ if, and only if, $\mathcal{O} (s) = 0$ and $\nexists t \in \mathcal{D}$ such that $s \mathcal{R} t$, $\mathcal{O} (t) = 0$ and $l(t) < l(s)$.
\end{theorem}
\begin{proof}
Let $N(s) = 0$ with $\mathcal{O}(s) = 0$ and assume that $\exists t \in \mathcal{D}$ such that $s \mathcal{R} t$, $\mathcal{O} (t) = 0$ and $l(t) < l(s)$. Then, by the axiom of surfeit we have that $N(t) < N(s) = 0$ which is a contradiction with the axiom of non-negativity.

Let $s \in \mathcal{D}$ such that $\mathcal{O} (s) = 0$ and that $\nexists t \in \mathcal{D}$, $s \mathcal{R} t$, $\mathcal{O} (t) = 0$ and $l(t) < l(s)$, and assume that $N(s) > 0$ (according to the axiom of non-negativity, it is not possible that $N(s) < 0$). By the axiom of existence of minimum we now that $\exists \, r \in \mathcal{D}$ such that $s \mathcal{R} r$ and $N(r) = 0$. We have to consider all possible cases:

\begin{enumerate}[label=(\roman*)]

\item If $\mathcal{O}(s) = \mathcal{O}(r)$ and $l(s) = l(r)$ by Proposition \ref{prop:properties_nescience}-(c) we have that $N(s) = N(r) = 0$ which is a contradiction with the fact that $N(s) > 0$.

\item If $\mathcal{O}(s) = \mathcal{O}(r)$ and $l(s) < l(r)$ by the axiom of surfeit we have that $N(s) < N(r) = 0$ which is a contradiction with the axiom of non-negativity.

\item It cannot be the case that $\mathcal{O}(s) = \mathcal{O}(r)$ and that $l(s) > l(r)$, since the theorem requires that an $r$ with those properties does not exists.

\item If $\mathcal{O}(s) < \mathcal{O}(r)$ and $l(s) = l(r)$  by the axiom of inaccuracy we have that $N(s) < N(r) = 0$ which is a contradiction with the axiom of non-negativity.

\item If $\mathcal{O}(s) < \mathcal{O}(r)$ and $l(s) < l(r)$ by Proposition \ref{prop:properties_nescience}-(e) we have that $N(s) < N(r) = 0$ which is a contradiction with the axiom of non-negativity.

\item If $\mathcal{O}(s) < \mathcal{O}(r)$ and $l(s) > l(r)$ by Proposition \ref{prop:zero_inaccuracy} we have that $\mathcal{O}(r) = 0$, and $\mathcal{O}(s) < \mathcal{O}(r) = 0$ is a contradiction with the fact that $\mathcal{O} (s) \geq 0$ according to the axiom of inaccuracy.

\item If $\mathcal{O}(s) > \mathcal{O}(r)$ and $l(s) = l(r)$ by Proposition \ref{prop:zero_inaccuracy} we have that $\mathcal{O}(r) = 0$, and so $\mathcal{O}(s) > \mathcal{O}(r) = 0$ which is a contradiction with the assumption of $\mathcal{O}(s) = 0$.

\item If $\mathcal{O}(s) > \mathcal{O}(r)$ and $l(s) < l(r)$ apply the same argument than in (vii).

\item If $\mathcal{O}(s) > \mathcal{O}(r)$ and $l(s) > l(r)$ apply the same argument than in (vii).

\end{enumerate}

\end{proof}

{\color{red} Introduce the following corollary}

\begin{corollary}
For every $s \in \mathcal{D}$ there exist and $r \in \mathcal{D}$ sucht that $r \mathcal{R} s$, $\mathcal{O} (r) = 0$ and $\nexists t \in \mathcal{D}$ such that $t \mathcal{R} r$, $\mathcal{O} (t) = 0$ and $l(t) < l(r)$.
\end{corollary}
\begin{proof}
It is a direct consequence of the axiom of existence of minimum and Theorem \ref{th:perfect_knowledge}.
\end{proof}


There is no limit on how much we can unknown about an entity. However, in practice we usually work with the normalized version of the nescience of topics, so we can compare how much we do not know about topics for different entities.

If might happen that the shortest possible description of a topic is longer than the topic itself. In that case, there is no way we can further reduce the nescience of a topic.

{\color{red} Introduce the concept of empty model, and prove its properties.}

{\color{red} Introduce the following definition.}

\begin{definition}
A \emph{scientific methodology} is an effective procedure that produces a sequence of $(t_1, m_1), (t_2, m_2), \ldots, (t_n, m_n), \ldots$ where $(t_i, m_i) \in \Sigma^\ast \times \mathcal{M}$ such that $N(t_i, m_i) < N(t_{i+1}, m_{i+1})$.
\end{definition}

Model Concatenation

\begin{proposition}
L
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

Conditional Knowledge

\begin{proposition}
L
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}



%
% Section: Science as a Language
%

\section{Science as a Language}

{\color{red} TODO: Provide an alternative definition of the set of descriptions, and prove that it is equivalent to our definition based on the description function}

Since we require computable descriptions, we would like to know if the set of all possible descriptions of a topic is computable as well.

\begin{proposition}
Study if $D_{t}$ is Turing-decidable, Turing-recognizable or none.
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

Although we know that it is not computable, we are interested in the set composed by the shortest possible description of each topic.

\begin{definition}
We define the set of \emph{perfect descriptions}, denoted by $\mathcal{D}^\star$, as:
\[
\mathcal{D}^\star = \{ d_t^\star : t \in \mathcal{T} \}
\]
\end{definition}

Since the set $\mathcal{D}$ includes all the possible descriptions of all the possible (describable) topics, we can see this set as a kind of language for science. We do not call it universal language since it depends on the initial set of entities $\mathcal{E}$ and the particular encoding used for these entities.

\begin{proposition}
The set $\mathcal{D}$ is not Turing-decidable.
\end{proposition}
\begin{proof}
{\color{red} TODO: Because it depends on the set $\mathcal{T}$ that it could be not computable}
\end{proof}

Say something here

\begin{proposition}
The set $\mathcal{D}$ is Turing-recognizable.
\end{proposition}
\begin{proof}
{\color{red} TODO: The same argument as the previous proposition}
\end{proof}

A universal language is determined by a universal Turing machine. Given Two different universal Turing machines $\delta_{a}$ and $\delta_{b}$ defines two different universal languages $\mathcal{L}_{a}$ and $\mathcal{L}_{b}$. Let $\mathcal{L}_{a=b}=\left\{ \left\langle d_{a},d_{b}\right\rangle \,,\,d_{a},d_{b}\in\mathcal{D}\mid\delta_{a}\left(d_{a}\right)=\delta_{b}\left(d_{b}\right)\right\}$.

\begin{proposition}
Study if $\mathcal{L}_{a=b}$ is Turing-decidable, Turing-recognizable or none.
\end{proposition}
\begin{proof}
{\color{red} TODO}
\end{proof}

Let $\mathcal{L}_{\nexists t}$ the laguage of valid descriptions (from a formal point of view) that do not describe any topic, that is, $\mathcal{L}_{\nexists t}=\left\{ d\in\mathcal{D}\mid\nexists t\in\mathcal{T}\,,\,\delta\left(d\right)=t\right\}$.

\begin{proposition}
Study if $\mathcal{L}_{\nexists t}$ is Turing-decidable, Turing-recognizable or none.
\end{proposition}

Let $\mathcal{L}_{\nexists d}$ the laguage topics that are not described by any description, that is, $\mathcal{L}_{\nexists d}=\left\{ t\in\mathcal{T}\mid\nexists d\in\mathcal{D}\,,\,\delta\left(d\right)=t\right\}$.

\begin{proposition}
Study if $\mathcal{L}_{\nexists d}$ is Turing-decidable, Turing-recognizable or none.
\end{proposition}

{\color{red} TODO: Extend the concept of conditional description to multiple topics.}

{\color{red} TODO: Introduce the concept of "independent topics" based on the complexity of the conditional description. Study its properties.}

{\color{red} TODO: Define a topology in $\mathcal{T}$ and $\mathcal{D}$. Study the continuity of $\delta$. Study the invariants under $\delta$.}


%
% Section: The Inaccuracy - Surfeit Trade-off
%

\section{The Inaccuracy - Surfeit Trade-off}

{\color{red} TODO: Explain that given a miscoding, there is a trade-off between inaccuracy and surfeit, in the sense that there exists an optimal point beyond it the more we decrease the inaccuracy, the more we increase the surfeit, and so, the nescience keep constant. Explain how this trade-off imposes a limit to knowledge.}

%
% Section: Science vs. Pseudoscience
%

\section{Science vs. Pseudoscience}

{\color{red} TODO: Provide a characterization of the difference between science and pseudoscience. In science, nescience decreases with time, in pseudoscience not.}

%
% Section: Graspness
%
\section{Graspness}

There are some topics whose nescience decrease with time much faster than others, even if the amount of research effort involved is similar. A possible explaination to this fact is that there are some topics that are inherently more difficult to understand.

We define the \emph{graspness} of a topic as the entropy of the set of possible descriptions of that topic. A high graspness means a difficult to understand topic. Intuitively, a research topic is difficult if it has no descriptions much shorter than the others, that is, descriptions that significantly decrease the nescience. For example, in physics there have been a sucession of theories that produced huge advances in our understanding of how nature works (Aristotelian physics, Newton physics, Einstein physics), meanwhile in case of philosophy of science, new theories (deductivism, inductivism, empirism, falsation, ...) have a similar nescience than previous ones.

\begin{definition}[Graspness]
Let $D_t$ the set of descriptions of a topic $t \in T$. The \emph{graspness} of topic $t$, denoted by $G(t)$, is defined by:
\[
G(t) = \sum_{d \in D_t} 2^{-l(d)} l(d)
\]
\end{definition}

Grapsness is a postive quantity, whose maximum is reached when all the descriptions of the topic have the same length:

\begin{proposition}
Let $D_t = \{ d_1, d_2, \ldots, d_q \}$ the set of descriptions of a topic $t \in T$, then we have that $G(t) \leq \log q$, and $G(t) = \log q$ if, and only if, $l(d_1) = l(d_2) = \ldots = l(d_q)$.
\end{proposition}
\begin{proof}
Replace $P(s_i)$ by $2^{-l(d_i)}$ in Proposition \ref{prop:maximum_entropy}.
\end{proof}

If $G(t) = \log q$ then we have that $N_t = 0$ for all $\hat{d}_t$. In that case, it does not make any sense to do research, since no knew knowledge can be adquired. This is the case of pseudosciences, like astrology, where the nescience of descriptions does not decrease with time. If fact, graspness can be used as a distintive element of what constitutes \emph{science} and what does not.

\begin{definition}
A topic $t \in T$ is \emph{scientable} if $G(t) < \log d(D_t) - \epsilon$, where $\epsilon \in \mathbb{R}$ is a constant.
\end{definition}

We can extend the concept of graspness from topics to areas, for example, by means of computing the average graspness of all the topics included in the area.

\begin{definition}
Let $A \subset T$ a research area. The \emph{graspness} of area $A$, denoted by $G(A)$, is defined by:
\[
G(A) = \frac{1}{d(A)} \sum_{t \in A} G(t)
\]
\end{definition}

%
% Section: Effort
%
\section{Effort}

{\color{red} TODO: Provide a characterization of the effort, measured in terms of number of operations, or time, to reduce the nescience. Based in the concept of computational complexity.}

%
% Section: Human Understanding
%
\section{Human Understanding}

{\color{red} TODO: Provide a model of the limitation of the humans to understand some scientific theories. Explain that we have to get used to the fact that we cannot understand the overhelming majority of scientific topics, and that computers can do.}

%
% Section: Areas in Decay
%

\section{Areas in Decay}

{\color{red} Provide a model of the decay in the interest of research areas. For example, a good explanatory variable could be the number of interesting questions: the less interesting questions, the more the area is near to its end as a interesting research area, of course, as long as its topics are not used as tools.}

%
% Section: References
%

\section*{References}

{\color{red} Mention the polemic between Hilbert and Fredge}

